# Intro & Getting Started

## 01 - Welcome to the Course

- just a general overview

---

## 02 - What is Bootstrap & Why Use It?

### What is Bootstrap?

- open source front-end framework for fast web dev
- pre-made HTML/CSS templates & classes
- JavaScript widgets and plugins
- mobile first, responsive layout

### Advantages

- Browser compatibility
- Faster development
- Good for devs that lack design/UI skills
- Easy to learn
- Responsive layouts

### Ways to Install/Use

- Include the CDN (used in this course)
- Download and include files (not recommended)
- NPM/Yarn

### Utilities

- Grid system
- Text and Typography
- Positioning
- Spacing
- Sizing
- Alignment
- Colors
- Images
- Responsive Utilities
- Shadows

### CSS Components

- Forms and Input Groups
- Tables
- List Groups
- Cards
- Progress Bars
- Alerts
- Navbars and Menus
- Buttons
- Pagination
- Media Objects
- Jumbotron
- Badges

### JavaScript Widgets

- Carousel Slider
- Collapsible Accordion
- Modals
- Tooltips
- Popovers
- ScrollSpy

---

## 03 - What's New In Bootstrap 4?

### Button Styles & Flat Design

- Flat style over gradient
- More subtlelook
- More color classes
- `.btn-outline-*` class

### Improved Grid System and Layout

- Changes in underlying architecture
- rem & em units instead of pixels
- New -xl tier for extra large screens
- Grid now uses Flexbox

### Flexbox

- Includes flexbox utilities
- Use flexbox to manage grids, navigation, components, and more
- Display utilities can transform child items into flex items
- `<div class="d-flex p-2">I'm a flexbox container!</div>`

### Cards

- Replace panels, wells, and thumbnails
- Use flexbox
- Get same behavior with modifiers
- As little markup as possible
- `.card-body` instead of `.well`
- Use `.card-title` for titles
- Image headings

### Changes to the Navbar

- Flat design
- Uses flexbox instead of floats
- Wrapping `.navbar` with `.navbar-expand{-sm|-md|-lg|-xl}` for responsive collapse
- Color schemes

### Form Changes

- Dropped the `.form-horizontal` class
- Forms using grids require the `.row` class and can use `.col-form-label` for labels
- Use `.form-control-lg` and `.form-control-sm` to increase/decrease the size of an input control
- Help text now uses the `.form-text` class instead of `.help-block`

### Sass CSS Pre-compiler

- Moved from Less to Sass
- As of v3, a Sass port was created and maintained
- Sass is favored in the webdev community
- Use of Libsass to compile faster

### Some Other Changes

- Bigger font and headings
- Dropped support for IE8 and IE9
- Reboot module
- Inversed tables (`.table-dark`)
- Glyphicons dropped (Font-awesome recommended)
- Sizing and spacing classes

---

## 04: Initial Environment Setup

- VSCode with Prettier and Live Server

### Some helpful Emmet shortcuts

- `lorem#40` + TAB generates 40 words of lorem ipsum
- `li*5` generates 5 list items

---

## 05: Bootstrap Sandbox Setup

---
